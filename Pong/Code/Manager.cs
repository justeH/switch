﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Manager : SwitchGameObject 
{
	public GameObject backGround;
	public GameObject playerPaddle;
	public GameObject ballToCreate;
	public GameObject cornerWall;
	public GameObject invisWall;
	private float currentTime = 0.0f;
	private GameObject timer;
	private GameObject leaderboard;
	private int ballcount = 0;
	private List<GameObject> Player = new List<GameObject>();
	private List<GameObject> backWall = new List<GameObject>();

	private Vector2 relativePos(float x, float y)
	{
		return new Vector2(x+transform.position.x, y+transform.position.y);
	}
	// Use this for initialization
	void Start () 
	{
		GameObject temp;
		timer = new GameObject();
		leaderboard = new GameObject();
		temp = (GameObject)Instantiate (backGround, relativePos(0.0f,0.0f), Quaternion.identity);
		temp.transform.parent = transform;
		temp = (GameObject)Instantiate (cornerWall, relativePos(-4.4f,-4.4f), Quaternion.identity);
		temp.transform.parent = transform;
		temp = (GameObject)Instantiate (cornerWall, relativePos(-4.4f,4.4f), Quaternion.Euler(0, 0, 270));
		temp.transform.parent = transform;
		temp = (GameObject)Instantiate (cornerWall, relativePos(4.4f, 4.4f), Quaternion.Euler(0, 0, 180));
		temp.transform.parent = transform;
		temp = (GameObject)Instantiate (cornerWall, relativePos(4.4f, -4.4f), Quaternion.Euler(0, 0, 90));
		temp.transform.parent = transform;
		temp = (GameObject)Instantiate (invisWall, relativePos(0.0f, -5.3f), Quaternion.identity);
		temp.transform.parent = transform;
		temp.GetComponent<wallScript>().setPlayerNum(1);
		backWall.Add (temp);
		temp = (GameObject)Instantiate (invisWall, relativePos(0.0f, 5.3f), Quaternion.identity);
		temp.transform.parent = transform;
		temp.GetComponent<wallScript>().setPlayerNum(2);
		backWall.Add (temp);
		temp = (GameObject)Instantiate (invisWall, relativePos(-5.3f, 0.0f), Quaternion.Euler(0, 0, 90));
		temp.transform.parent = transform;
		temp.GetComponent<wallScript>().setPlayerNum(3);
		backWall.Add (temp);
		temp = (GameObject)Instantiate (invisWall, relativePos(5.3f, 0.0f), Quaternion.Euler(0, 0, 90));
		temp.transform.parent = transform;
		temp.GetComponent<wallScript>().setPlayerNum(4);
		backWall.Add (temp);

		List<Vector2> tempPos = new List<Vector2>();
		Vector2 tempVect = new Vector2(0.0f, -4.8f);
		tempPos.Add(tempVect);
		tempVect = new Vector2(0.0f, 4.8f);
		tempPos.Add(tempVect);
		tempVect = new Vector2(-4.8f, 0.0f);
		tempPos.Add(tempVect);
		tempVect = new Vector2(4.8f, 0.0f);
		tempPos.Add(tempVect);
		for(int i = 0; i < 4; i++)
		{
			GameObject tempPlayer;
			if(i >= 2)
			{
				tempPlayer = (GameObject)Instantiate(playerPaddle, relativePos(tempPos[i].x, tempPos[i].y), Quaternion.Euler(0, 0, 90));
				tempPlayer.GetComponent<Paddle_Control>().setPlayerVert(true);
			}
			else
			{
				tempPlayer = (GameObject)Instantiate(playerPaddle, relativePos(tempPos[i].x, tempPos[i].y), Quaternion.identity);
				tempPlayer.GetComponent<Paddle_Control>().setPlayerVert(false);
			}
			tempPlayer.GetComponent<Paddle_Control>().setPlayerNum(i+1);
			tempPlayer.transform.parent = transform;
			Player.Add(tempPlayer);
		}
		//GameObject aball = (GameObject)Instantiate(ballToCreate, new Vector2(0.0f, 0.0f), Quaternion.identity);
		timer.AddComponent<GUIText>();
		timer.transform.position = new Vector3(0.1f,0.9f,0.0f);
		timer.guiText.text = "Time: ";	
		leaderboard.AddComponent<GUIText>();
		leaderboard.transform.position = new Vector3(0.84f,0.45f,0.0f);
		//leaderboard.guiText.text = "<size=40><color=red>Player 1: " + Leaderboard.scores["Pong"][0] + "</color>\n<color=green>Player 2: " + Leaderboard.scores["Pong"][1] + "</color>\n<color=blue>Player 3: " + Leaderboard.scores["Pong"][2] + "</color>\n<color=yellow>Player 4: " + Leaderboard.scores["Pong"][3] + "</color></size>";

	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(!paused)
		{
			leaderboard.guiText.text = "<size=30><color=#9B4343>Player 1: " + Leaderboard.scores["Pong"][0] + "</color>\n<color=#79983F>Player 2: " + Leaderboard.scores["Pong"][1] + "</color>\n<color=#407E99>Player 3: " + Leaderboard.scores["Pong"][2] + "</color>\n<color=#CEB245>Player 4: " + Leaderboard.scores["Pong"][3] + "</color></size>";
		}
		if(!checkGameOver())
		{
			if (!paused) 
			{
				timer.guiText.enabled = true;
				leaderboard.guiText.enabled = true;
				currentTime += Time.deltaTime;
				int intTime = (int)currentTime;
				timer.guiText.text = "<color=black><size=45>" + intTime + "</size></color>";
				if(intTime/10 >= ballcount)
				{
					GameObject aball = (GameObject)Instantiate(ballToCreate, relativePos(0.0f, 0.0f), Quaternion.identity);
					aball.transform.parent = transform;
					aball.GetComponent<Move_Ball>().center = relativePos (0.0f,0.0f);
					aball.transform.position = relativePos (0.0f,0.0f);
					if(intTime%30 == 0 && intTime != 0)
					{
						swapPlayers();
					}
					ballcount += 1;
				}
			}
			else
			{
				timer.guiText.enabled = false;
				leaderboard.guiText.enabled = false;
			}
		}
		else
		{
			timer.guiText.text = "<color=white><size=40>Time: GAME OVER</size></color>";
			List<GameObject> gameObjects = new List<GameObject>(GameObject.FindGameObjectsWithTag("Pong"));

			for(int i = 0; i < gameObjects.Count; i++)
			{
				Destroy (gameObjects[i]);
			}
		}
	}
	void swapPlayers()
	{
		List<int> nums = new List<int>(){1,2,3,4};
		for(int i = 0; i < 4; i++)
		{
			int rand = Random.Range(0,4-i);
			Player[i].GetComponent<Paddle_Control>().setPlayerNum(nums[rand]);
			backWall[i].GetComponent<wallScript>().setPlayerNum(nums[rand]);
			nums.RemoveAt(rand);
		}
	}

	bool checkGameOver()
	{
		for(int i = 0; i < 4; i++)
		{
			if(Leaderboard.scores["Pong"][i] >= 3)
			{
				return true;
			}
		}
		return false;
	}
}
