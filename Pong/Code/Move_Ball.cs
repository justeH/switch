﻿using UnityEngine;
using System.Collections;

public class Move_Ball : SwitchGameObject 
{
	private Vector2 randDirection;
	private int player = 0;
	public int ballForce = 200;
	public int maxballForce = 600;
	public float maxSpeed = 0.1f;
	public Vector2 center;
	public GameObject target;
	// Use this for initialization

	/*
	void Start()
	{

	}*/

	void Awake()
	{
		moveBall();
	}

	void FixedUpdate ()
	{

	}

	public void moveBall()
	{
		float x = chanceGenerator();
		float y = chanceGenerator();

		renderer.material.color = Color.white;
		player = 0;
		ballForce = 200;
		randDirection = new Vector2 (x,y);
		Debug.Log(transform.parent);
		transform.position = new Vector3(-22.0f,0.0f,0.0f);
		randDirection = randDirection.normalized;
		rigidbody2D.velocity = randDirection * ballForce * Time.fixedDeltaTime;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		Vector2 paddlePosition = coll.gameObject.transform.position;
		Vector2 ballPosition = gameObject.transform.position;
		
		// this is the vector 'pointing' from the paddle to the ball
		Vector2 delta = ballPosition - paddlePosition;
		if(coll.gameObject.GetComponent<Paddle_Control>() != null)
		{
			player = coll.gameObject.GetComponent<Paddle_Control>().getPlayerNum();
			renderer.material.color = Leaderboard.getColour(player);
		}
		// normalizing converts a vector into a unit vector
		// (i.e. a vector with the same direction, but a magnitude of 1)
		Vector2 direction = delta.normalized;

		if(ballForce < maxballForce)
		{
			ballForce += 80;
		}
		// set the velocity to be the direction vector scaled to the desired speed
		rigidbody2D.velocity = direction * ballForce * Time.fixedDeltaTime;
	}

	float chanceGenerator()
	{
		return Random.Range (-360, 360);
	}

	public int getPlayer()
	{
		return player;
	}
}
