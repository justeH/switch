﻿using UnityEngine;
using System.Collections;

public class Paddle_Control : SwitchGameObject 
{
	private int player = 1;
	private bool vertical = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{	
		if (!paused) 
		{
			if(vertical == false)
			{
				rigidbody2D.velocity = new Vector2(Mathf.Lerp(0, Input.GetAxis("Pong_Horizontal"+player.ToString())*10*Time.deltaTime*60, 0.8f),0.0f);
			}
			else
			{
				rigidbody2D.velocity = new Vector2(0.0f, Mathf.Lerp(0, -Input.GetAxis("Pong_Vertical"+player.ToString())*10*Time.deltaTime*60, 0.8f));
			}
		}
	}

	public void setPlayerNum(int newnum)
	{
		player = newnum;
		renderer.material.color = Leaderboard.getColour (player);
	}

	public int getPlayerNum()
	{
		return player;
	}

	public void setPlayerVert(bool newvert)
	{
		vertical = newvert;
	}
}
