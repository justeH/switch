﻿using UnityEngine;
using System.Collections;

public class wallScript : SwitchGameObject 
{
	public GameObject ballObject;
	private int player;
	// Use this for initialization
	void Start () 
	{
		renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//renderer.material.color = Leaderboard.getColour(player);
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.GetComponent<Move_Ball>() != null)
		{
			int num = coll.gameObject.GetComponent<Move_Ball>().getPlayer();
			if(num > 0)
			{
				if(num != player)
				{
					Leaderboard.scores["Pong"][num-1]++;
					//Leaderboard.scores["Pong"][player-1]--;
					if(Leaderboard.scores["Pong"][player-1] < 0)
					{
						Leaderboard.scores["Pong"][player-1] = 0;
					}
				}
				/*else
				{
					Leaderboard.scores["Pong"][player-1]-=1;
					if(Leaderboard.scores["Pong"][player-1] < 0)
					{
						Leaderboard.scores["Pong"][player-1] = 0;
					}
				}*/
			}
			coll.gameObject.GetComponent<Move_Ball>().moveBall();
		}
	}

	public void setPlayerNum(int newplayer)
	{
		player = newplayer;
	}
}
