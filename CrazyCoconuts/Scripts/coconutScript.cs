﻿using UnityEngine;
using System.Collections;

public class coconutScript : SwitchGameObject
{
	private int playerUsed;
	// Use this for initialization
	void Start () 
	{
		rigidbody2D.velocity = new Vector2(0, -5);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	public void setPlayer(int newPlayer)
	{
		playerUsed = newPlayer;
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		/*
		if(coll.gameObject.tag == "Player")
		{
			float tempX = coll.gameObject.transform.position.x;
			float tempY = coll.gameObject.transform.position.y;
			Vector2 tempOriginal = GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().returnPlayer(playerUsed);
			int collidedPlayer = coll.gameObject.GetComponent<coconutControl>().getPlayer();
			GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().swapPlayer(playerUsed,tempX, tempY);
			GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().swapPlayer(collidedPlayer,tempOriginal.x, tempOriginal.y);
			//GameObject.AddComponent (
		}*/
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.gameObject.name == "RunnerCreator(Clone)")
		{

			float tempX = transform.position.x;
			float tempY = transform.position.y;

			if(GameObject.FindGameObjectWithTag("MainCoconut") != null)
			{
				GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().swapPlayer(playerUsed,tempX, tempY);
				GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().setIn(playerUsed);
			}
			Destroy (coll.gameObject);
		}
		
		if(coll.gameObject.name == "Player(Clone)" && coll.gameObject.transform.position.y < 0)
		{
			float tempX = coll.gameObject.transform.position.x;
			float tempY = coll.gameObject.transform.position.y;
			Vector2 tempOriginal = GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().returnPlayer(playerUsed);
			int collidedPlayer = coll.gameObject.GetComponent<coconutControl>().getPlayer();
			GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().swapPlayer(playerUsed,tempX, tempY);
			GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().swapPlayer(collidedPlayer,tempOriginal.x, tempOriginal.y);
			GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().setIn(playerUsed);
			GameObject.FindGameObjectWithTag("MainCoconut").GetComponent<CoconutManager>().setIn(collidedPlayer);
			//GameObject.AddComponent (
		}
		
		if(coll.gameObject.name == "wall(Clone)")
		{
			Destroy (gameObject);
		}
	}
}
