﻿using UnityEngine;
using System.Collections;

public class coconutControl : SwitchGameObject
{
	private int player = 1;
	private bool isIn = false;
	public GameObject coconutObject;
	public float coconutDelay = 4.0f;
	private float nextCoconut = 0.0f;
	
	// Use this for initialization
	void Start ()
	{
		
	}

	void FixedUpdate()
	{
		if(Input.GetKeyDown ("joystick " + player + " button 0") && isIn == false && Time.time >= nextCoconut)
		{
			nextCoconut = Time.time + coconutDelay;
			float x = transform.position.x;
			float y = transform.position.y;
			GameObject createCoconut = (GameObject)Instantiate (coconutObject, new Vector2 (x, y), Quaternion.identity);
			createCoconut.GetComponent<coconutScript>().setPlayer(player);
		}
		rigidbody2D.velocity = new Vector2(Mathf.Lerp(0, Input.GetAxis("Pong_Horizontal"+player.ToString())*10*Time.deltaTime*60, 0.8f),0.0f);
		
		if(Input.GetAxis("Pong_Horizontal"+player.ToString()) < 0.01 && Input.GetAxis("Pong_Horizontal"+player.ToString()) > -0.01)
		{
			rigidbody2D.velocity = Vector2.zero;
		}
	}

	public void setPlayerNum(int aPlayer)
	{
		player = aPlayer;
	}

	public int getPlayer()
	{
		return player;
	}
	
	public void setIn()
	{
		if(isIn == false)
		{
			isIn = true;
		}
		else
		{
			isIn = false;
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		Debug.Log ("It collided");
		
	}
}