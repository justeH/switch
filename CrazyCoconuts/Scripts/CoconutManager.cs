﻿using UnityEngine;
using System.Collections;

public class CoconutManager : SwitchGameObject
{
	public GameObject playerToCreate;
	public GameObject background;
	public GameObject maxWall;
	public GameObject beginObject;
	//public GameObject go;
	
	private float currentTime = 0.0f;
	private GameObject timer;
	private GameObject leaderboard;
	private GameObject loot;
	private GameObject Player_1;
	private GameObject Player_2;
	private GameObject Player_3;
	private GameObject Player_4;
	
	// Use this for initialization
	void Start () 
	{
		timer = new GameObject();
		leaderboard = new GameObject();
		timer.AddComponent<GUIText>();
		timer.transform.position = new Vector3(0.1f,0.9f,0.0f);
		leaderboard.AddComponent<GUIText>();
		leaderboard.transform.position = new Vector3(0.84f,0.45f,0.0f);
		Player_1 = (GameObject)Instantiate(playerToCreate, relativePos(Random.Range(-5.0f, 5.0f), 4.2f), Quaternion.identity);
		Player_1.GetComponent<coconutControl>().setPlayerNum(1);
		Player_1.renderer.material.color = Color.red;
		Player_1.transform.parent = transform;

		Player_2 = (GameObject)Instantiate(playerToCreate, relativePos(Random.Range(-5.0f, 5.0f), 4.2f), Quaternion.identity);
		Player_2.GetComponent<coconutControl>().setPlayerNum(2);
		Player_2.renderer.material.color = Color.blue;
		Player_2.transform.parent = transform;

		Player_3 = (GameObject)Instantiate(playerToCreate, relativePos(Random.Range(-5.0f, 5.0f), 4.2f), Quaternion.identity);
		Player_3.GetComponent<coconutControl>().setPlayerNum(3);
		Player_3.renderer.material.color = Color.green;
		Player_3.transform.parent = transform;

		Player_4 = (GameObject)Instantiate(playerToCreate, relativePos(Random.Range(-5.0f, 5.0f), 4.2f), Quaternion.identity);
		Player_4.GetComponent<coconutControl>().setPlayerNum(4);
		Player_4.renderer.material.color = Color.yellow;
		Player_4.transform.parent = transform;

		Physics2D.IgnoreCollision(Player_1.collider2D, Player_2.collider2D);
		Physics2D.IgnoreCollision(Player_1.collider2D, Player_3.collider2D);
		Physics2D.IgnoreCollision(Player_1.collider2D, Player_4.collider2D);
		Physics2D.IgnoreCollision(Player_2.collider2D, Player_1.collider2D);
		Physics2D.IgnoreCollision(Player_2.collider2D, Player_3.collider2D);
		Physics2D.IgnoreCollision(Player_2.collider2D, Player_4.collider2D);
		Physics2D.IgnoreCollision(Player_3.collider2D, Player_1.collider2D);
		Physics2D.IgnoreCollision(Player_3.collider2D, Player_2.collider2D);
		Physics2D.IgnoreCollision(Player_3.collider2D, Player_4.collider2D);
		Physics2D.IgnoreCollision(Player_4.collider2D, Player_1.collider2D);
		Physics2D.IgnoreCollision(Player_4.collider2D, Player_2.collider2D);
		Physics2D.IgnoreCollision(Player_4.collider2D, Player_3.collider2D);
		
		GameObject Blackground = (GameObject)Instantiate (background, relativePos(0, 0), Quaternion.identity);
		Blackground.transform.parent = transform;
		
		GameObject beginCreature = (GameObject)Instantiate (beginObject, relativePos(Random.Range(-5.0f, 5.0f), -4.5f), Quaternion.identity);
		beginCreature.transform.parent = transform;

		GameObject Wall_Up = (GameObject)Instantiate(maxWall, relativePos(0, 5.5f), Quaternion.Euler(0, 0, 90));
		Wall_Up.transform.parent = transform;
		GameObject Wall_Down = (GameObject)Instantiate(maxWall, relativePos(0, -5.5f), Quaternion.Euler(0, 0, 90));
		Wall_Down.transform.parent = transform;
		GameObject Wall_Left = (GameObject)Instantiate(maxWall, relativePos(-5.5f, 0), Quaternion.identity );
		Wall_Left.transform.parent = transform;
		GameObject Wall_Right = (GameObject)Instantiate(maxWall, relativePos(5.5f, 0), Quaternion.identity);
		Wall_Right.transform.parent = transform;


		//timer.AddComponent<GUIText>();
		//timer.transform.position = relativePos(0.01f,0.9f);
		//leaderboard.AddComponent<GUIText>();
		//leaderboard.transform.position = new Vector3(0.8f,0.8f,0.0f);
		//go.AddComponent<GUIText>();
		//go.transform.position = new Vector3(0.1f,0.1f,0.0f);
		//go.guiText.text = "Hello World";	
	}
	
	private Vector2 relativePos(float x, float y)
	{
		return new Vector2(x+transform.position.x, y+transform.position.y);
	}
	
	public Vector2 returnPlayer(int playerNum)
	{
		if(playerNum == 1)
			return Player_1.transform.position;
		else if(playerNum == 2)
			return Player_2.transform.position;
		else if(playerNum == 3)
			return Player_3.transform.position;
		else if(playerNum == 4)
			return Player_4.transform.position;
			
		return Vector2.zero;
	}
	
	public void swapPlayer(int playerNum, float x, float y)
	{
		if(playerNum == 1)
			Player_1.transform.position = new Vector2(x,y);
		if(playerNum == 2)
			Player_2.transform.position = new Vector2(x,y);
		if(playerNum == 3)
			Player_3.transform.position = new Vector2(x,y);
		if(playerNum == 4)
			Player_4.transform.position = new Vector2(x,y);
	}
	
	public void setIn(int playerNum)
	{
		if(playerNum == 1)
			Player_1.GetComponent<coconutControl>().setIn();
		if(playerNum == 2)
			Player_2.GetComponent<coconutControl>().setIn();
		if(playerNum == 3)
			Player_3.GetComponent<coconutControl>().setIn();
		if(playerNum == 4)
			Player_4.GetComponent<coconutControl>().setIn();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FixedUpdate () 
	{
		if(!paused)
		{
			timer.guiText.enabled = true;
			leaderboard.guiText.enabled = true;
			currentTime += Time.deltaTime;
			int intTime = (int)currentTime;
			timer.guiText.text = "<color=black><size=45>" + intTime + "</size></color>";
			leaderboard.guiText.text = "<size=30><color=#9B4343>Player 1: " + Leaderboard.scores["Coconuts"][0] + "</color>\n<color=#79983F>Player 2: " + Leaderboard.scores["Coconuts"][1] + "</color>\n<color=#407E99>Player 3: " + Leaderboard.scores["Coconuts"][2] + "</color>\n<color=#CEB245>Player 4: " + Leaderboard.scores["Coconuts"][3] + "</color></size>";
		}
		else
		{
			timer.guiText.enabled = false;
			leaderboard.guiText.enabled = false;
		}
		//timer.guiText.text = "<color=white><size=40>Time: " + intTime + "</size></color>";
		
		//leaderboard.guiText.text = "<size=40><color=red>Player 1: " + Leaderboard.scores["Pong"][0] + "</color>\n<color=green>Player 2: " + Leaderboard.scores["Pong"][1] + "</color>\n<color=blue>Player 3: " + Leaderboard.scores["Pong"][2] + "</color>\n<color=yellow>Player 4: " + Leaderboard.scores["Pong"][3] + "</color></size>";
	}

	void OnCollisionStay2D(Collision2D coll)
	{
		//Leaderboard.scores["King"][coll.gameObject.GetComponent<playerControl>().getPlayer()]++;

	}
}
