﻿using UnityEngine;
using System.Collections;

public class initialCreature : SwitchGameObject 
{
	public float creatureSpeed = 200.0f;
	private bool direction = false;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		
		if(direction == false)
		{
			rigidbody2D.AddForce(Vector3.right * creatureSpeed * Time.deltaTime * 15);
		}
		else
		{
			rigidbody2D.AddForce(-Vector3.right * creatureSpeed * Time.deltaTime * 15);
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if(direction == false)
			direction = true;
		else
			direction = false;
	}
}
