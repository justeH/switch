﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

public class PivotController : SwitchGameObject{
	// Normal Movements Variables
	public float curSpeed;
	public float maxSpeed;
	public Transform target;
	public int playerNumber;
	public GameObject projectile;
	
	void Start()
	{
		curSpeed = 1;
		maxSpeed = 10;
	}

	void Update(){

	}

	
	void FixedUpdate()
	{
		if (!paused) 
		{
			float currInput = Input.GetAxis ("Asteroid_Horizontal" + playerNumber.ToString ());
			transform.Rotate (Vector3.forward, -currInput * 10);
		}
	}
}