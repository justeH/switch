﻿using UnityEngine;
using System.Collections;

public class EarthController : SwitchGameObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D objectYouCollidedWith) 
	{
		if(objectYouCollidedWith.gameObject.GetComponent<AsteroidController>() != null)
		{
			int playernumber = objectYouCollidedWith.gameObject.GetComponent<AsteroidController>().playerValue;
			if (Leaderboard.scores ["Asteroids"] [playernumber - 1] > 0) 
			{
				Leaderboard.scores ["Asteroids"] [playernumber - 1]--;
			}
			Destroy (objectYouCollidedWith.gameObject);
		}
	}
}