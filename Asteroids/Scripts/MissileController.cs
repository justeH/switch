﻿using UnityEngine;
using System.Collections;

public class MissileController : SwitchGameObject {

	public GameObject target;
	public GameObject source;

	// Use this for initialization
	void Start () {

			Vector3 dir =  Vector3.zero - transform.position;
			dir = dir.normalized;
			dir = -dir;
			rigidbody2D.AddForce (dir * 300);
	
	}

	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.x > 6 || this.transform.position.y > 6 || this.transform.position.x < -6 || this.transform.position.y < -6) {

			Destroy(gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D objectYouCollidedWith) 
	{
		if(objectYouCollidedWith.gameObject.GetComponent<AsteroidController>() != null)
		{
			int playernumber = objectYouCollidedWith.gameObject.GetComponent<AsteroidController>().playerValue;
			Leaderboard.scores ["Asteroids"] [playernumber - 1]++;

			if (Leaderboard.scores ["Asteroids"] [playernumber - 1] >= 15) {
				Leaderboard.scores ["Main"] [playernumber - 1] ++;
				GameObject[] objects = GameObject.FindGameObjectsWithTag("Asteroids");

				foreach (GameObject obj in objects){
					Destroy (obj);
				}
				Destroy(gameObject);
			}

			Destroy (objectYouCollidedWith.gameObject);
			Destroy (gameObject);
		}
	}
}
