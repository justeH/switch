﻿using UnityEngine;
using System.Collections;

public class AsteroidController : SwitchGameObject {

	public GameObject target;
	public int playerValue;
	private float rotationalValue;
	public Sprite yellowSprite;
	public Sprite greenSprite;
	public Sprite blueSprite;
	public Sprite redSprite;

	// Use this for initialization
	void Start () {
			switch (playerValue) {
			case 1:
				GetComponent<SpriteRenderer>().sprite = redSprite;
				//this.renderer.material.color =  Color.red;
				break;
			case 2: 
				GetComponent<SpriteRenderer>().sprite = greenSprite;
				break;
			case 3: 
				GetComponent<SpriteRenderer>().sprite = blueSprite;
				break;
			case 4: 
				GetComponent<SpriteRenderer>().sprite = yellowSprite;
				break;
			}

		if (target != null) {
				Vector3 dir = target.transform.position - transform.position;
				dir = dir.normalized;

				//generate random offset to ensure it doesn't always head for the center of the target
				float offsetX = Random.Range (0.0f, 0.1f);
				float offsetY = Random.Range (0.0f, 0.1f);
				float forceVal = Random.Range (10.0f, 50.0f);

				//rotationalValue = Random.Range (-50f, 50f);
			float angle = Mathf.Atan2(target.transform.position.y - transform.position.y, target.transform.position.x - transform.position.x) * 180 / Mathf.PI;
				transform.Rotate (0, 0, angle+90);

				Vector3 adjustedDir = new Vector3 (dir.x + offsetX, dir.y + offsetY);

				rigidbody2D.AddForce (adjustedDir * forceVal);
		} 
	}

	void OnCollisionEnter2D(Collision2D objectYouCollidedWith) { 

	}
	

	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.x > 6 || this.transform.position.y > 6 || this.transform.position.x < -6 || this.transform.position.y < -6) {

			Destroy(gameObject);
		}
	}

	void FixedUpdate () {

	}
}
