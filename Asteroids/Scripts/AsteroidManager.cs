﻿using UnityEngine;
using System.Collections;

public class AsteroidManager : SwitchGameObject {

	public GameObject backgroundPrefab;
	public GameObject earthPreFab;
	public GameObject playerPrefab;
	public GameObject asteroidPrefab;
	public ArrayList players;
	private GameObject timer;
	private GameObject leaderboard;
	private float currentTime = 0.0f;
	public GameObject earth;

	// Use this for initialization
	void Start () {
		leaderboard = new GameObject();
		timer = new GameObject();
		GameObject background = (GameObject)Instantiate (backgroundPrefab);
		background.transform.parent = transform;
		earth = (GameObject)Instantiate (earthPreFab);
		earth.transform.parent = transform;
		earth.transform.position = background.transform.position;
		background.transform.position = new Vector3(
			background.transform.position.x,
			background.transform.position.y,
			background.transform.position.z+5);

		float rotation = 0.0f;

		ArrayList players = new ArrayList ();

		for (int i = 1; i < 5; i++) {
			GameObject player = (GameObject)Instantiate(playerPrefab);
			player.transform.parent = transform;
			player.transform.position = earth.transform.position;
			player.transform.Rotate(new Vector3(0,0,rotation));
			playerPrefab.GetComponent<PivotController>().playerNumber = i;
			players.Add(player);
			rotation += 90.0f;
		}

		Invoke ("Spawn", 3.0f);
		timer.AddComponent<GUIText>();
		timer.transform.position = new Vector3(0.1f,0.9f,0.0f);
		leaderboard.AddComponent<GUIText>();
		leaderboard.transform.position = new Vector3(0.84f,0.45f,0.0f);
	
	}
	
	void Spawn (){
		if (!paused)
		{
			for (int i = 1; i < 5; i++){ 
				float spawnRotation = Random.Range (0f, 359.0f);
				
				GameObject asteroidInstance = (GameObject)Instantiate (asteroidPrefab);
				
				asteroidInstance.transform.position = getCirclePosition (spawnRotation, 5.5f );
				asteroidInstance.transform.parent = transform;
				AsteroidController controller = asteroidInstance.GetComponent<AsteroidController> ();
				controller.target = earth;
				controller.playerValue = i;

			}
		}
		float spawnTime = Random.Range (5.0f, 5.0f);
		Invoke ("Spawn", spawnTime);
	}
	
	Vector2 getCirclePosition(float degrees, float radius){
		float x = 0;
		float y = 0;
		float radians = 0;
		
		radians = degrees * Mathf.PI / 180.0f;
		
		x = radius * Mathf.Cos(radians);
		y = radius * Mathf.Sin(radians);
		
		Vector2 position = new Vector2(x, y);
		return position;
	}
	
	// Update is called once per frame
	void Update () {
		if(!paused)
		{
			leaderboard.guiText.enabled = true;
			timer.guiText.enabled = true;
			currentTime += Time.deltaTime;
			int intTime = (int)currentTime;
			timer.guiText.text = "<color=black><size=45>" + intTime + "</size></color>";
			leaderboard.guiText.text = "<size=30><color=#9B4343>Player 1: " + Leaderboard.scores["Asteroids"][0] + "</color>\n<color=#79983F>Player 2: " + Leaderboard.scores["Asteroids"][1] + "</color>\n<color=#407E99>Player 3: " + Leaderboard.scores["Asteroids"][2] + "</color>\n<color=#CEB245>Player 4: " + Leaderboard.scores["Asteroids"][3] + "</color></size>";
		}
		else
		{
			leaderboard.guiText.enabled = false;
			timer.guiText.enabled = false;
		}
	}
}
