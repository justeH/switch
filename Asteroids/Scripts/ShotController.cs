﻿using UnityEngine;
using System.Collections;

public class ShotController : SwitchGameObject {

	public GameObject projectile;
	public GameObject target;
	public int playerNumber;
	public Sprite yellowSprite;
	public Sprite greenSprite;
	public Sprite blueSprite;
	public Sprite redSprite;

	private Sprite playerSprite;

	void Start()
	{
		playerNumber = transform.parent.GetComponent<PivotController> ().playerNumber;
		switch (playerNumber) {
		case 1:
			playerSprite = redSprite;
			break;
		case 2: 
			playerSprite = greenSprite;
			break;
		case 3: 
			playerSprite = blueSprite;
			break;
		case 4: 
			playerSprite = yellowSprite;
			break;

		}

		GetComponent<SpriteRenderer>().sprite = playerSprite;
	}
	
	void Fire() {
		GameObject shot = (GameObject)Instantiate (projectile);
		shot.GetComponent<SpriteRenderer> ().sprite = playerSprite;
		Vector3 position = target.transform.position;
		float angle = Mathf.Atan2(position.y - transform.position.y, position.x - transform.position.x) * 180 / Mathf.PI;
		shot.transform.Rotate (0, 0, angle+90);
		shot.transform.parent = transform.parent.parent;
		shot.transform.position = this.transform.position;
	}
	
	
	void Update()
	{
		if (!paused) 
		{
			if (Input.GetButtonDown ("Asteroid_Shoot" + playerNumber.ToString())) {
				Fire();
			}
		}
		
	}
}
